package controller

import (
	"first-gomod/model"
	"first-gomod/service"
	"first-gomod/util"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetMessage(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}

func GetMessageHelloWorld(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "Hello World",
	})
}

func InsertBook(c *gin.Context) {
	var book model.Book
	c.BindJSON(&book)

	book, err := service.Insert(book)

	c.JSON(200, util.GenerateDefaultResponse(err, 0, "", book))
}

func FindBookById(c *gin.Context) {
	var book model.Book

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)

	if err != nil {
		c.JSON(400, util.GenerateDefaultResponse(err, 0, "", ""))
	} else {
		data, err := service.FindById(id, book)
		c.JSON(200, util.GenerateDefaultResponse(err, 0, "", data))
	}
}
