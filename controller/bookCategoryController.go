package controller

import (
	"first-gomod/model"
	"first-gomod/service"
	"first-gomod/util"
	"strconv"

	"github.com/gin-gonic/gin"
)

func InsertBookCategory(c *gin.Context) {
	var bCategory model.BookCategory
	c.BindJSON(&bCategory)

	bCategory, err := service.Insert(bCategory)

	c.JSON(200, util.GenerateDefaultResponse(err, 0, "", bCategory))
}

func FindBookCategoryById(c *gin.Context) {
	var book model.BookCategory

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)

	if err != nil {
		c.JSON(400, util.GenerateDefaultResponse(err, 0, "", ""))
	} else {
		data, err := service.FindById(id, book)
		c.JSON(200, util.GenerateDefaultResponse(err, 0, "", data))
	}
}
