package test

import (
	"first-gomod/application"
	"first-gomod/controller"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBookController(t *testing.T) {
	r := application.LoadGin()

	r.GET("/test/hello-world", controller.GetMessageHelloWorld)

	mockResponse := `{"message":"Hello World"}`
	req, _ := http.NewRequest("GET", "/test/hello-world", nil)

	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	res, _ := ioutil.ReadAll(w.Body)
	assert.Equal(t, mockResponse, string(res))
}
