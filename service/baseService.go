package service

import (
	"context"
	"database/sql"
	"first-gomod/config"
	"first-gomod/repository"
	"fmt"

	"github.com/uptrace/bun"
)

func LoadContext() context.Context {
	return context.Background()
}

func DB() *bun.DB {
	return config.CreateDatabaseConnection()
}

func StartDB() (context.Context, *bun.DB) {
	return context.Background(), config.CreateDatabaseConnection()
}

func Insert[T any](t T) (T, error) {

	// ctx := LoadContext()
	// trx, err := DB().BeginTx(ctx, &sql.TxOptions{})
	ctx, db := StartDB()
	trx, err := db.BeginTx(ctx, &sql.TxOptions{})

	t, err = repository.InsertOne(ctx, trx, t)

	if err != nil {
		fmt.Println(err)
		err = trx.Rollback()
	} else {
		err = trx.Commit()
	}

	return t, err
}

func FindById[T any](id int64, t T) (T, error) {
	// ctx := LoadContext()
	// db := DB()
	ctx, db := StartDB()
	t, err := repository.FindById(ctx, db, id, t)

	fmt.Println(t)

	return t, err
}
