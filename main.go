package main

import (
	"first-gomod/application"

	"github.com/joho/godotenv"
)

func main() {

	// Load environment variable
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}

	// Load Application
	application.LoadApplication()

}
