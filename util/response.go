package util

import "github.com/gin-gonic/gin"

func GenerateDefaultResponse[T any](err error, statusCode int, message string, data T) gin.H {
	if err != nil {
		if statusCode == 0 {
			statusCode = 400
		}
		return gin.H{
			"statusCode":   statusCode,
			"status":       "error",
			"message":      message,
			"errorMessage": err,
			"data":         nil,
		}
	} else {
		if statusCode == 0 {
			statusCode = 200
		}
		return gin.H{
			"statusCode":   statusCode,
			"status":       "success",
			"message":      "",
			"errorMessage": nil,
			"data":         data,
		}
	}

}
