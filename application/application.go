package application

import (
	"os"

	"first-gomod/config"
	"first-gomod/router"

	"github.com/gin-gonic/gin"
)

func LoadGin() *gin.Engine {
	return gin.Default()
}

func LoadApplication() {

	r := LoadGin()

	// init database call, remove this if no need using database
	config.InitDatabaseConnection()

	router.LoadRouters(r)

	// listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	url := os.Getenv("APP_URL") + ":" + os.Getenv("APP_PORT")
	r.Run(url)

}
