package config

import (
	"context"
	"database/sql"
	"first-gomod/model"
	"fmt"
	"os"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"github.com/uptrace/bun/extra/bundebug"
)

/**
 * Core function to connect database before using bun as DB connector
 */
func BuildPostgresConn() (pgdb *sql.DB) {

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable&read_timeout=120s", dbUser, dbPass, dbHost, dbPort, dbName)
	fmt.Println(dsn)
	pgdb = sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	return
}

func InitDatabaseConnection() {
	conn := BuildPostgresConn()
	err := conn.Ping() // ping server
	db := bun.NewDB(conn, pgdialect.New())
	if err != nil {
		panic(err)
	}

	// export schema
	ExportSchema(db)

}

func CreateDatabaseConnection() (db *bun.DB) {
	conn := BuildPostgresConn()
	// Create a Bun db on top of it.
	db = bun.NewDB(conn, pgdialect.New())
	// Print all queries to stdout.
	db.AddQueryHook(bundebug.NewQueryHook(bundebug.WithVerbose(true)))

	return
}

func ExportSchema(db *bun.DB) {

	ctx := context.Background()

	// use same code to generate tables
	_, err := db.NewCreateTable().Model((*model.Book)(nil)).
		// ForeignKey(`("author_id") REFERENCES "users" ("id") ON DELETE CASCADE`).
		Exec(ctx)

	/*
		_, err = db.NewCreateTable().Model((*model.BookCategory)(nil)).
			Exec(ctx)
		_, err = db.NewCreateTable().Model((*model.User)(nil)).
			Exec(ctx)

		_, err = db.NewCreateTable().Model((*model.Role)(nil)).
			Exec(ctx)

		_, err = db.NewCreateTable().Model((*model.UserRole)(nil)).
			Exec(ctx)
	*/

	if err != nil {
		fmt.Println(err)
	}
}
