package repository

import (
	"context"
	"fmt"

	"github.com/uptrace/bun"
)

func InsertOne[T any](ctx context.Context, trx bun.Tx, t T) (T, error) {
	_, err := trx.NewInsert().Model(&t).Exec(ctx)

	fmt.Println(t)

	return t, err
}

func FindById[T any](ctx context.Context, db *bun.DB, id int64, t T) (T, error) {
	// ids := strconv.FormatInt(id, 10)
	err := db.NewSelect().
		Model(&t).
		Where("id = ? ", id).
		Scan(ctx)

	return t, err
}
