package router

import (
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func LoadRouters(r *gin.Engine) {

	r.Use(CORS())
	CallRouters(r)

}

/*
 * Load your routers here
 */
func CallRouters(r *gin.Engine) {

	BookRouter(r)
	// AuthRouter(r)

}

/*
 * Define all necessary CORS set up here
 */
func CORS() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowOrigins:     []string{"https://foo.com"},
		AllowMethods:     []string{"OPTIONS", "GET", "POST", "DELETE", "PUT"},
		AllowHeaders:     []string{"Origin", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},
		MaxAge: 12 * time.Hour,
	})
}
