package router

import (
	"first-gomod/controller"

	"github.com/gin-gonic/gin"
)

func BookRouter(r *gin.Engine) {
	bookRouter := r.Group("/books")
	{
		// test route only
		bookRouter.GET("/ping", controller.GetMessage)
		bookRouter.GET("/hello-world", controller.GetMessageHelloWorld)

		// real functions
		// bookRouter.POST("/", controller.InsertBook)
		// bookRouter.GET("/book/:id", controller.FindBookById)
		// bookRouter.POST("/category", controller.InsertBookCategory)
		// bookRouter.GET("/category/:id", controller.FindBookCategoryById)
	}
}
