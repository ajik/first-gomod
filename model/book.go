package model

import "time"

type Book struct {
	tableName     struct{}  `bun:"book"`
	ID            int64     `json:"id" bun:"id,pk,autoincrement"`
	Title         string    `json:"title,omitempty" bun:"title"`
	Description   string    `json:"description,omitempty" bun:"description"`
	Status        string    `json:"status,omitempty" bun:"status"`
	ISBN          string    `json:"ISBN,omitempty" bun:"ISBN,notnull,unique"`
	Remark        string    `json:"remark,omitempty" bun:"remark"`
	DeletedAt     time.Time `bun:"deleted_at,soft_delete,nullzero"`
	CreatedAt     time.Time `json:"created_at" bun:"created_at,default:current_timestamp"`
	UpdatedAt     time.Time `json:"updated_at" bun:"updated_at,default:current_timestamp"`
	LastUpdatedBy string    `bun:"last_updated_by"`
}
