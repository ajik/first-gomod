package model

import "time"

type BookCategory struct {
	tableName     struct{}  `bun:"book_category"`
	ID            int64     `json:"id" bun:"id,pk,autoincrement"`
	Category      string    `json:"category,omitempty" bun:"category"`
	Description   string    `json:"description,omitempty" bun:"description"`
	DeletedAt     time.Time `bun:"deleted_at,soft_delete,nullzero"`
	CreatedAt     time.Time `json:"created_at" bun:"created_at,default:current_timestamp"`
	UpdatedAt     time.Time `json:"updated_at" bun:"updated_at,default:current_timestamp"`
	LastUpdatedBy string    `bun:"last_updated_by"`
}
